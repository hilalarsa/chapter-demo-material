class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, my name1 is ${this.name}`);
  }

  work() {
    console.log("Work!");
  }
}

// Create a child class from Human
class Programmer extends Human {
  constructor(name, address, programmingLanguages) {
    super(name, address);
    /* Call the super/parent class constructor,
        in this case Person.constructor; */
    this.programmingLanguages = programmingLanguages;
  }

  // //   method override
  introduce(withDetail) {
    // super.introduce();
    // // Call the super class introduce instance method.
    // console.log(`I can write `, this.programmingLanguages);

    // Overlaoding (With detail)
    console.log(Array.isArray(withDetail))
    Array.isArray(withDetail)
      ? console.log(`I can write withDetail ${withDetail}`) // true
      : console.log("Wrong input"); // false
  }
  // code() {
  //   console.log(
  //     "Code some",
  //     this.programmingLanguages[
  //       Math.floor(Math.random() * this.programmingLanguages.length)
  //     ]
  //   );
  // }
}

// Initiate from Human directly
let Rafi = new Human("Rafi Ahmad", "Isekai");
// Rafi.introduce(); // Hi, my name is Barack Ahmad
console.log(Rafi)
console.log(Rafi.introduce())
// let Rafatar = new Programmer("Rafatar", "Jakarta", [
//   "Javascript",
//   "Kotlin",
//   "Python",
// ]);
// Rafatar.introduce(['Javascript']); // Hi, my name is Rafatar; I can write ["Javascript", "Kotlin", "Python"]
// Rafatar.code(); // Code some Javascript/Ruby/...
// Rafatar.work(); // Call super class method that isn't overrided or overloaded

// try {
//   // Rafi can't code since Rafi is an direct instance of Human, which don't have code method
//   Rafi.code(); // Error: Undefined method "code"
// } catch (err) {
//   console.log("error gan");
//   console.log(err.message);
// }

// console.log(Rafatar instanceof Human); // true
// console.log(Rafatar instanceof Programmer); // true

// const programmingLanguages = ["Javascript", "Kotlin", "Python"];
// const textCode = programmingLanguages[
//   Math.floor(Math.random() * programmingLanguages.length)
// ];
// // console.log(Math.PI)
// // console.log(Math.floor(1.01))
// console.log(Math.floor(Math.random() * programmingLanguages.length))
// console.log(textCode)
