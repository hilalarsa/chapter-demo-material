class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }
  work() {
    console.log(`${this.constructor.name}:`, "Working!");
  }
}

class Writer extends Human {
  work() {
    console.log("Write books");
    // super.work();
  }
}

class Doctor extends Human {
  work() {
    console.log("Treat with medicine");
    // super.work();
  }
}

const Junji = new Writer("Junji", "Japan");
const Boyke = new Doctor("Boyke", "Jakarta");
console.log(Junji.work());
console.log(Boyke.work());