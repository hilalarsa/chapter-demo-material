const object = {
    name: "Country",
    countries: ["Sweden", "USA", "Indonesia"]
}

const arrayOfObject = [{
    id: 1,
    title: "Sweden",
    status: "eliminated"
}, {
    id: 2,
    title: "USA",
    status: "eliminated"
}, {
    id: 3,
    title: "England",
    status: "eliminated"
}]

const array = [1,2,3,4]


console.log(typeof object)
console.log(typeof array)
console.log(typeof arrayOfObject)