// Variable declaration
var $ = 10; // $ sebagai nama variabel
let _ = 100; // _ sebagai nama variabel

// console.log($);
// console.log(_);


// Var Assignment
/* Buat variabel var dengan cara deklarasi dulu */
var harga; // Declaration
harga = 1000; // Assignment

/* Buat variabel var yang langsung kita kasih nilai */
var harga = 1000


// Var Scoping
var diskon = 500 // Global scope
if (true){
 var diskon = 300 // Global scope
}
// console.log(diskon) 

/* Sebelum ada ES6, solusinya membuat function scope -> local scope */
let flower = "mawar" // Global scope
function diskonScope(){
    console.log(flower) 
    let flower = "tulip" // Local scope
}
diskonScope() 
console.log(flower) 


// LET Reassign


// CONST Assigment
const WARNA_MERAH = "#F00";
const WARNA_BIRU = "#00F";
const WARNA_HIJAU = "#0F0";

/* Ketika kita ingin memanggil warna
  Kita cuma perlu panggil variabelnya saja */
let warnaBaju = WARNA_MERAH;
console.log(warnaBaju);


// CONST Re-Assigment
/* Kita gak bisa ubah nilai bumi */
const bumi = "bulat";
bumi = "datar";

/* Kita juga gak bisa deklarasi ulang */
// const bumi = "datar";

// Reassign object / array

/* Object dengan variabel const masih bisa kita ubah property-nya */
const obj = { id:1, name:'Sabrina'}
obj.location="Jakarta"
console.log(obj) // Output: { id:1, name:'Sabrina', location:'Jakarta'}
// Tapi, kita gak bisa reassigned
obj={} // Output: TypeError: Assignment to constant variable.

/* Array dengan variabel const masih bisa kita ubah property-nya */
const arr = [1,2,3,4]
arr.push(5)
console.log(arr) // Output: [1,2,3,4,5]
// Tapi, kita gak bisa reassigned
arr=[] // Output: TypeError: Assignment to constant variable.