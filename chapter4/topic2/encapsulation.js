// ===Public
// class Human {
//   constructor(name, address) {
//     this.name = name;
//     this.address = address;
//   }
//   // This is public instance method
//   introduce() {
//     console.log(`Hello, my name is ${this.name}`);
//   }
//   // This is public static method
//   static isEating(food) {
//     let foods = ["plant", "animal"];
//     return foods.includes(food.toLowerCase());
//   }
// }

// let mj = new Human("Michael Jackson", "Isekai");
// console.log(mj);
// // Output: Human {name: "Michael Jackson", address: "Isekai"}
// console.log(Human.isEating("Human")); // false
// console.log(Human.isEating("Plant")); // true
// console.log(mj.name); // true
// console.log(Human.introduce("Plant")); // true

// =======

// ===Private
class Human {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  //   Private function
  #doGossip = () => {
      console.log(`My address will become viral ${this.address}`);
    };
    talk() {
        console.log(this.#doGossip()); // Call the private method
    }
    static #isHidingArea = true;
}

let mj = new Human("Michael Jackson", "Isekai");
console.log(mj.talk()); // Will run, won't return error!
// // Output: My address will become viral Isekai
// try {
//   console.log(Human.#isHidingArea); // Will return an error!
//   mj.#doGossip(); // Won't run, will return error!
// } catch (err) {
//   console.error(err);
// }
// // Private field '#isHidingArea' must be declared in an enclosing class
// // ======

// // ==== Protected
// class Human {
//     constructor(name, address) {
//      this.name = name;
//      this.address = address;
//    }
//     _call() {
//      console.log(`Call me as a ${this.name}`)
//     }
//    }

//    class Programmer extends Human {
//     constructor(name, address, task, salary) {
//       super(name, address);
//       this.task = task;
//       this.salary = salary;
//     }
//     doCall() {
//       super._call() // Will run
//     }
//    }

//    let sb = new Human("Sabrina", "Jakarta");
//    let job = new Programmer("Developer", "$1000");
//    console.log(sb._call()) // Call me as a Sabrina
//    /*Meskipun ini gak error ketika kita panggil protected secara public. Tapi, kita harus paham method ini protected, yang semestinya hanya boleh dipanggil di dalam class declaration atau sub-classnya.*/
//    console.log(job.doCall()) // Call me as a Developer

// // =========
