// function multipleParams(a,b,c){
//   console.log(a)
//   console.log(b)
//   console.log(c)
// }

// const params = [1,2,3]

// multipleParams(...params)


// const parts = ['shoulders', 'knees'];
// const lyrics = ['head', ...parts, 'and', 'toes'];



// const arr = [1, 2, 3];
// const arr2 = [...arr]; // like arr.slice()





// // Concat an array
// let arr1 = [0, 1, 2];
// const arr2 = [3, 4, 5];

// // Append all items from arr2 onto arr1
// arr1 = arr1.concat(arr2);
// let arr1 = [0, 1, 2];
// const arr2 = [3, 4, 5];

// arr1 = [...arr1, ...arr2];
// // arr1 is now [0, 1, 2, 3, 4, 5]



// // Object spread
// const obj1 = { foo: 'bar', x: 42 };
// const obj2 = { foo: 'baz', y: 13 };

// const clonedObj = { ...obj1 };
// // { foo: "bar", x: 42 }

// const mergedObj = { ...obj1, ...obj2 };
// // { foo: "baz", x: 42, y: 13 }



// let string = "Loremipsum dolor sit amet"
// console.log(string.split(' '))
// for(let i=0; i < string.length; i++){
//   console.log(string[i])
// }
