// Function Declaration (ES5)
function volTabung(r, t) {
  return 3.14 * r ** 2 * t;
}
console.log("Volume Tabung:", volTabung(10, 4));
// Volume Tabung: 1256

// Function Expression
const volTabung = function (r, t) {
  return 3.14 * r ** 2 * t;
};
console.log("Volume Tabung:", volTabung(10, 4));
// Volume Tabung: 1256

// Arrow Function (ES6)
const volTabung = (r, t) => 3.14 * r ** 2 * t;
console.log("Volume Tabung:", volTabung(10, 4));
// Volume Tabung: 1256

Change 1