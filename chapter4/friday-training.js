var arr = [5,1,10,2,4]
var arr2 = [5]


var merged = [...arr, arr2, 100]
// console.log(merged)

var a = 1
var b = a


// var obj = {
//     nama: "bob",
//     umur: 12
// }
// var obj2 = {...obj}

// console.log(obj)
// console.log(obj2)

// obj2.nama = "bob2"

// console.log(obj)
// console.log(obj2)

// var obj = {
//     nama: "bob",
//     umur: 12
// }

// var namaVar = obj.nama
// var umurVar = obj.umur

// var {nama: namaVar, umur: umurVar} = obj
// console.log(namaVar)
// console.log(umurVar)

// var a = prompt()
// console.log(a)


// var array = [1, 2, 3, 4]
// var result = array.map(function(item, index){
//     if(item < 3){
//         return item
//     }else{
//         return ''
//     }
// })

// var result2 = array.map((item, index) => (item < 3 ? item : ''))


var worldCup = [
    {
        country: "Marocco",
        win: 5,
        lose: 3,
        worldCupWins: 0,
    },
    {
        country: "Argentina",
        win: 5,
        lose: 3,
        worldCupWins: 2,
    },
    {
        country: "Crotaia",
        win: 4,
        worldCupWins: 0,
        lose: 4,
    },
    {
        country: "France",
        win: 4,
        worldCupWins: 2,
        lose: 3,
    },
]

document.getElementById("worldcup").innerHTML = worldCup.map(item => 
    `<ul>
        <li> Country: ${item.country}</li>
        <li> Win: ${item.win}</li>
        <li> Lose: ${item.lose}</li>
        <li> World Cup Wins: ${item.worldCupWins}</li>
    </ul>
    `
    ).join('')
