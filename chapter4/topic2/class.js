// class Person {
//   constructor(name, address) {
//     this.name = name;
//     this.address = address;
//   }
// }

// console.log(Person);
// console.log(typeof Person);

class Human {
  // Add static property
  isLivingOnMars = false;
  static isLivingOnEarth = true;

  // Add constructor method
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  // Add instance method signature
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  static sayHi() {
    console.log(`Hi, ${this.name}`);
  }
}

// let bob = new Human("name", "address");
// bob.introduce();
// bob.sayHi();
// Human.sayHi()

console.log(Human.isLivingOnEarth)
console.log(Human.isLivingOnMars)

// console.log(Human.isLivingOnEarth);
// console.log(Human.isLivingOnMars);
// Output static property: true

// Add prototype/instance method
// Human.prototype.greet = function (name) {
//   console.log(`Hi, ${name}, I'm ${this.name}`);
// };

// // Add static method
// Human.destroy = function (thing) {
//   console.log(`Human is destroying ${thing}`);
// };

// // Instantiation of Human class, we create a new object.
// let mj = new Human("Michael Jackson", "Isekai");
// console.log(mj);
// // Output: Human {name: "Michael Jackson", address: "Isekai"}
// // Checking instance of class
// console.log(mj instanceof Human); // true
// console.log(mj.introduce());
// // Hi, my name is Michael Jackson
// console.log(mj.greet("Donald Trump"));
// // Hi, Donald Trump, I'm Michael Jackson
// console.log(Human.destroy("Amazon Forest"));
// // Human is destroying Amazon Forest

// console.log(mj.introduce());
