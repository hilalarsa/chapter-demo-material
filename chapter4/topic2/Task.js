const strArray = ["JavaScript", "Java", "C"]; // Data array 3 elemen

function forEach(array, callback) {
  // 2 parameter, array & callback
  const newArray = []; // < array kosong
  console.log(array);

  for (let i = 0; i < array.length; i++) {
    // perulangan thdp array.length
    console.log(array);
    console.log(newArray);
    newArray.push(callback(array[i]));
  }
  console.log(array);
  console.log(newArray);
  return newArray;
}

// run forEach, argumen 1 variable strArray, argumen 2 callback, mereturn
const lenArray = forEach(strArray, (item) => {
  return item.length;
});
// const lenArray = forEach(strArray, ("JavaScript") => { return item.length; });

// const lenArray = forEach(strArray, (item) => item.length);
// const lenArray = forEach(strArray, function(item){
//     return item.length
// });
console.log(lenArray);
